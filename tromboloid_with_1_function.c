//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main() {
    float h, b, d, volume;
    printf("Enter the value of h : ");
    scanf("%f", &h);
    printf('Enter the value of d : ");
    scanf("%f", &d);
    printf("Enter the value of b : ");
    scanf("%f", &b);
    volume = ((0.33) * ((h * d) + d)) / b; 
    printf("Volume of tromboloid is %f", volume);
    return 0;
}
