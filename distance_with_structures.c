//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct point {
    float x, y;
};

typedef struct point Pt;

Pt input() {
    Pt p;
    printf("Enter abscissa ");
    scanf("%f",&p.x);
    printf("Enter ordinate ");
    scanf("%f",&p.y);
    return p;
}

 
void distance(struct point m, struct point n) {
    float dist;
    dist = sqrt(pow((m.x-n.x),2)+pow((m.y-n.y),2));
    printf("Distance between (%f, %f) and (%f, %f) is %f.", m.x, m.y, n.x, n.y, dist);
}
 
int main() {
    Pt m, n;
    m = input();
    n = input();
    distance(m, n);
    return 0;
}
