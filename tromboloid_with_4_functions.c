//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>

float input() {
    float x;
    printf("Enter the value : ");
    scanf("%f", &x);
    return x;
}

float volume(float x, float y, float z) {
    float volume = ((0.33) * ((x * y) + y)) / z;
    return volume;
}

void output(float vol) {
    printf("Volume of tromboloid is %f", vol);
}

int main() {
    float h, d, b, vol;
    h = input();
    d = input();
    b = input();
    vol = volume(h, d, b);
    output(vol);
    return 0;
}