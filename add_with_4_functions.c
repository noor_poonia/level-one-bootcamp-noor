//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int input()
{
    int x; 
    printf("Enter a number\n");
    scanf("%d", &x);
    return x;
}

int sum(int a, int b)
{
    int sum;
    sum = a + b;
    return sum;
}

void output(int a, int b, int c)
{
    printf("Sum of %d + %d is %d\n",a,b,c);
}

int main()
{
    int a, b, s;
    a = input();
    b = input();
    s = sum(a, b);
    output(a, b, s);
    return 0;
}
